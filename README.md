# Neuroepithelium Cell Classification

## Data Description

Images of electroporated and stained mouse brain slices. Electroporated cells express GFP in green channel, nuclei are stained with DAPI in blue channel, later immunostaining stains different target in read and far-red channels.

## Goal of Analysis

1. Segment, count and look at spatial distribution of GPF-positive cells across brain layers.
2. Of the GFP-positive cells, estimate fraction of cells positive for signal in other channels.
3. Look at statistical distribution across conditions of the above two metrics.

## Workflow

1. Cells were loaded and organized in a singla [QuPath](https://qupath.github.io/) project.
2. GFP-positive cells were segmented inside a manually-defined rectangle using cyto2 model of cellpose, through the [BIOP-cellpose-qupath plugin](https://github.com/BIOP/qupath-extension-cellpose).
3. Manually-defined rectangle was divided into 10 rectangular sections along its long axis, and the number of GFP-positive cells were counted per sub-section and exported to a single csv file using QuPath detection measurements.
4. A background ROI was drawn for all channels for which classification was needed, in a region where only background/aspecific/negative signal was present.
5. All properties of all detected GFP-positive cells and background ROIs were exported from QuPath and analyzed with Python as described below.
6. The mean intensity of all measured cells was subtracted with the corresponding mean value of its background ROI, to try to normalize all cells around similar intensities.
7. KMeans clustering or Otsu automatic thresholding method was used to find a intensity threshold to classify each GFP-positive cell into positive or negative for the other channels.
8. Number of positive, negative, double positive and double negative cells were expressed in percentage for the different conditions, and corresponding figures were exported.

## Content

Only the Jupyter notebooks used for the Python analysis are in this repository. QuPath scripts for the few operations described above will be added later on.

## Author
Marco Dalla Vecchia dallavem@igbmc.fr